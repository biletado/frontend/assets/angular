# Assets

## Runtime configuration

Place a file `assets/config.json` with an instance of [`{ RuntimeConfig } from 'app/app.runtimeconfig.ts`](src/app/app.runtimeconfig.ts) for runtime-configuration.

## Environment variables

| name          | default                            | description                                   |
|---------------|------------------------------------|-----------------------------------------------|
| BASE_HREF     | `` (empty string)                  | `<base href="$BASE_HREF">`                    |
| STATIC_PREFIX | unset, inherited from `$BASE_HREF` | path prefix where static files will be served |

If you serve the frontend without a path-prefix, no variable has to be set.

If you have a path-prefix which is not stripped, you need to set
`BASE_HREF` to `/your-prefix/`.

If you strip or change the path-prefix in your reverse-proxy-configuration/ingress-route,
you need to set `BASE_HREF` to `/your-prefix-viewed-from-browser`/ and
`STATIC_PREFIX` to `/your-prefix-in-actual-http-request/` or the empty string.

Both variables MAY contain leading or trailing `/`.

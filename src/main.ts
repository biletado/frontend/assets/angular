import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { AppComponent } from './app/app.component';
import { RUNTIME_CONFIG } from "./app/app.runtimeconfig";

fetch('/assets/config.json', {cache: "reload"})
  .then((response) => response.json())
  .then((config) => {
    const {providers, ...rest} = appConfig;

    bootstrapApplication(AppComponent, {
      ...rest,
      providers: [...providers, { provide: RUNTIME_CONFIG, useValue: config }],
    }).catch((err) => console.error(err));
  })

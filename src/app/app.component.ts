import {Component, inject, Inject} from '@angular/core';
import {RouterLink, RouterOutlet} from '@angular/router';
import {RUNTIME_CONFIG, RuntimeConfig} from "./app.runtimeconfig";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'assets';
  runtimeConfig: RuntimeConfig | null;
  apiUrl: string | undefined;
  constructor() {
    this.runtimeConfig = inject(RUNTIME_CONFIG, {optional: true})
    this.apiUrl = this.runtimeConfig?.apiUrl;
  }
}

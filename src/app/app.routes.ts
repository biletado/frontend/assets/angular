import { Routes } from '@angular/router';
import {AssetsComponent} from "./assets/assets.component";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";

export const routes: Routes = [
  {path: 'assets', component: AssetsComponent},
  {path: '**', component: PageNotFoundComponent},
];

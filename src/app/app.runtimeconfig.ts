import { InjectionToken } from '@angular/core';

export interface RuntimeConfig {
  /**
   * contains an absolute or relative URI to the api-endpoint
   */
  apiUrl: string
}

export let RUNTIME_CONFIG = new InjectionToken<RuntimeConfig>('RUNTIME_CONFIG')

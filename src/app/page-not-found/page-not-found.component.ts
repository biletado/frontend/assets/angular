import {Component, inject, PLATFORM_ID} from '@angular/core';
import {RESPONSE} from "../../express.tokens";
import {Response} from "express";
import {isPlatformServer} from "@angular/common";

@Component({
  selector: 'app-page-not-found',
  standalone: true,
  imports: [],
  templateUrl: './page-not-found.component.html',
  styleUrl: './page-not-found.component.scss'
})
export class PageNotFoundComponent {
  response: Response|null = inject(RESPONSE, {optional: true})
  platformId: Object = inject(PLATFORM_ID)

  //noinspection JSUnusedGlobalSymbols
  ngOnInit(){
    if(isPlatformServer(this.platformId) && this.response){
      this.response.status(404);
    }
  }
}

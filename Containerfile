FROM node:22-alpine AS base

FROM base AS package
WORKDIR /app
COPY package-lock.json* ./
COPY package.json ./package.orig.json
RUN node -e "const fs = require('fs'); const json = JSON.parse(fs.readFileSync('./package.orig.json')); delete json.version; fs.writeFileSync('./package.json', JSON.stringify(json, null, 2));"

# Install dependencies only when needed
FROM base AS deps
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat
WORKDIR /app

# Install dependencies based on the preferred package manager
COPY --from=package /app/package.json /app/package-lock.json* ./
RUN  npm ci

# Rebuild the source code only when needed
FROM base AS builder
WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .

RUN npm run build

FROM node:22-alpine

WORKDIR /usr/app

COPY --from=builder /app/dist/assets/ ./

ENV BASE_HREF=""

# this expects to have `exec "$@"` in the last line of `docker-entrypoint.sh`
RUN sed -i "\$ i\BASE_HREF_NORMALIZED=\"\$(echo \$BASE_HREF | sed -re \"s~^/+|/+$~~g\")/\"" /usr/local/bin/docker-entrypoint.sh
RUN sed -i "\$ i\[ \"\$BASE_HREF_NORMALIZED\" = \"/\" ] && BASE_HREF_NORMALIZED=\"\"" /usr/local/bin/docker-entrypoint.sh
RUN sed -i "\$ i\sed -i \'s~<base href=\"/\">~<base href=\"'\"/\$BASE_HREF_NORMALIZED\"'\">~' server/index.server.html" /usr/local/bin/docker-entrypoint.sh

CMD node server/server.mjs

EXPOSE 4000
